import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Home, Detail, Splash, Success, Login, Register } from '../pages';
import { useSelector } from 'react-redux';


const Stack = createNativeStackNavigator();

const NeedAuth = () => {
  return (
    <Stack.Navigator initialRouteName='Splash' >
      <Stack.Screen name='Splash' component={Splash} options={{ headerShown: false }} />
      <Stack.Screen name='Home' component={Home} options={{ headerShown: false }} />
      <Stack.Screen name='Detail' component={Detail} options={{ headerShown: false }} />
    </Stack.Navigator>
  )
}
const NoAuth = () => {
  return (
    <Stack.Navigator initialRouteName='Splash' >
      <Stack.Screen name='Splash' component={Splash} options={{ headerShown: false }} />
      <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
      <Stack.Screen name='Register' component={Register} options={{ headerShown: false }} />
      <Stack.Screen name='Success' component={Success} options={{ headerShown: false }} />
    </Stack.Navigator>
  )
}

const Router = () => {
  const token = useSelector((state) => state.Login.token);
  return (
    <>
      {token ? <NeedAuth /> : <NoAuth />}
    </>
  )
}

export default Router

const styles = StyleSheet.create({})