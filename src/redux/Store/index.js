import AsyncStorage from "@react-native-async-storage/async-storage";
import { applyMiddleware, combineReducers, createStore } from "redux";
import reduxLogger from "redux-logger";
import { persistReducer, persistStore } from "redux-persist";
import reduxThunk from 'redux-thunk';
import { 
    getDetailReducer,  
    loginReducer, 
    registerReducer, 
    getHomeReducer
} from "../Reducers";

const persistConfig = {
    key: 'root',
    storage: AsyncStorage,
    whitelist: ['Login']
};

const rootReducer = {
    Login: loginReducer,
    register: registerReducer,
    home: getHomeReducer,
    getDetail: getDetailReducer,
}

const configPersist = persistReducer(persistConfig, combineReducers(rootReducer))

export const Store = createStore(configPersist, applyMiddleware(reduxThunk, reduxLogger))

export const PersistStore = persistStore(Store)