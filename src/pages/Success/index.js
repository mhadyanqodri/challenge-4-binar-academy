import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'
import React from 'react'
import { success } from '../../assets'
import Login from '../Login'

const Success = ({navigation}) => {
  return (
    <View style={styles.container}>
      <View style={styles.complateContainer}>
        <Text style={styles.completeText}>Registration Completed!</Text>
      </View>
      <View style={styles.images}>
        <Image source={success} style={styles.successImage}></Image>
      </View>
      <View style={styles.verification}>
        <Text style={styles.verificationText}>We sent email verification to your email</Text>
      </View>
      <View style={styles.button}>
        <TouchableOpacity onPress={() => navigation.navigate(Login)}>
          <View style={styles.buttonContainer}>
            <Text style={styles.buttonText}>Back to login</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default Success

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  successImage: {
    width: 180,
    height: 180
  },
  completeText: {
    color: 'black',
    fontSize: 25,
    fontWeight: 'bold'
  },
  complateContainer: {
    alignItems: 'center',
    marginTop: 100
  },
  images: {
    alignItems: 'center',
    marginTop: 80
  },
  verification: {
    alignItems: 'center',
    marginTop: 30,
  },
  verificationText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'black'
  },
  buttonContainer: {
    backgroundColor: 'black',
    width: 350,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10
  },
  buttonText: {
    fontSize: 17,
    fontWeight: 'bold',
    color: 'white'
  },
  button: {
    alignItems: 'center',
    marginTop: 180
  }
})