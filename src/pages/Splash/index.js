import { StyleSheet, Text, View, ImageBackground } from 'react-native'
import React, { useEffect } from 'react'
import { BookApp } from '../../assets'
import { useSelector } from 'react-redux';

const Splash = ({ navigation }) => {
    const token = useSelector((state) => state.Login.token);

    useEffect(() => {
        setTimeout(() => {
            if (token) {
                navigation.replace('Home');
            } else {
                navigation.replace('Login');
            }
        }, 2500)
    }, [navigation]);

    return (
        <View style={styles.container}>
            <ImageBackground source={BookApp} style={styles.logo}></ImageBackground>
            <Text style={styles.aplicationName}>Book App</Text>
            <Text style={styles.creator}>M Hadyan Qodri</Text>
        </View>
    )
}

export default Splash

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'black'
    },
    logo: {
        width: 150,
        height: 150
    },
    aplicationName: {
        color: 'white',
        fontSize: 28,
        fontWeight: 'bold'
    },
    creator: {
        fontSize: 13,
        position: 'absolute',
        bottom: 18,
        color: 'white'
    }
})