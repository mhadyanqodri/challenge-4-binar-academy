import { StyleSheet, View, TouchableOpacity, Dimensions, Text } from 'react-native'
import React from 'react'
import Icon from 'react-native-vector-icons/FontAwesome';

const OthersDescription = ({ average_rating, total_sale, price }) => {
    return (
        <View style={styles.othersWrapper}>
            <View style={styles.rating}>
                <View style={styles.wrapper}>
                    <Text style={styles.titleLabel}>Rating</Text>
                    <Text style={styles.labelNumber}>{average_rating} <Icon name='star' size={21} color="gold"></Icon></Text>
                </View>
            </View>
            <View style={styles.totalSale}>
                <View style={styles.wrapper}>
                    <Text style={styles.titleLabel}>Total Sale</Text>
                    <Text style={styles.labelNumber}>{total_sale}</Text>
                </View>
            </View>
            <View style={styles.Button}>
                <View style={styles.wrapper}>
                    <TouchableOpacity>
                        <View style={styles.buyButton}>
                            <Text style={styles.price}>Buy {price}</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

export default OthersDescription

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    othersWrapper: {
        flexDirection: 'row',
        marginLeft: 15,
        alignItems: 'center',
        justifyContent: 'center'
      },
      wrapper: {
        marginRight: 13,
        alignItems: 'center'
      },
      titleLabel: {
        color: 'black',
        fontWeight: 'bold',
        fontSize: 18
      },
      labelNumber: {
        fontSize: 21,
        color: 'black',
      },
      buyButton: {
        backgroundColor: '#3A3845',
        width: windowWidth * 0.4,
        height: windowHeight * 0.04,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8,
        borderWidth: 1
      },
      price: {
        color: 'white'
      },
})