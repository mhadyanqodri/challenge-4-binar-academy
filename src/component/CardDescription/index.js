import { StyleSheet, View, Image, Dimensions, Text } from 'react-native'
import React from 'react'

const CardPopular = ({ source, title, author, publisher }) => {
    return (
        <View style={styles.descriptionWrapper}>
            <Image source={source} style={styles.bookImage}></Image>
            <View style={styles.titleWrapper}>
                <Text style={styles.title}>{title}</Text>
                <Text style={styles.textLabel}>{author}</Text>
                <Text style={styles.textLabel}>{publisher}</Text>
            </View>
        </View>
    )
}

export default CardPopular

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    descriptionWrapper: {
        flexDirection: 'row',
        paddingHorizontal: windowWidth * 0.07,
        paddingVertical: windowHeight * 0.03,
    },
    bookImage: {
        width: windowWidth * 0.38,
        height: windowHeight * 0.32,
        borderRadius: 10,
        marginLeft: 8,
        flex: 4
    },
    titleWrapper: {
        marginLeft: 15,
        marginTop: 5,
        flex: 4,
    },
    title: {
        color: 'black',
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom: 10
    },
    textLabel: {
        color: 'black',
        fontSize: 14,
        marginBottom: 5
    },
})