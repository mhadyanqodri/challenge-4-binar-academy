import { StyleSheet, View, TouchableOpacity, Image, Dimensions } from 'react-native'
import React from 'react'

const CardRecomended = ({ source, onPress }) => {
    return (
        <TouchableOpacity onPress={onPress}>
            <View style={styles.recomendedContainer}>
                <Image source={source} style={styles.recomendedCard}></Image>
            </View>
        </TouchableOpacity>
    )
}

export default CardRecomended

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    recomendedCard: {
        width: windowWidth * 0.35,
        height: windowHeight * 0.28,
        borderRadius: 10,
    },
    recomendedContainer: {
        marginTop: 15,
    }
})