import CardRecomended from './CardRecomended';
import CardPopular from './CardPopular';
import CardDescription from "./CardDescription";
import OthersDescription from "./OthersDescription";
import InternetOffline from "./InternetOffline";


export {
    CardRecomended,
    CardPopular,
    CardDescription,
    OthersDescription,
    InternetOffline,
};