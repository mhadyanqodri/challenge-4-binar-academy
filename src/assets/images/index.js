import BookApp from './BookApp.png';
import Encanto_ from './Encanto_.jpg';
import encanto from './encanto.jpg';
import turningRed from './TurningRed.jpg';
import turning_red from './turning_red.jpg';
import spiderMan from './spiderMan.jpg';
import spider_man from './spider_man.jpg';
import theBatman from './theBatman.jpg';
import the_batman from './the_batman.jpg';
import adamProject from './adamProject.jpg';
import adam_project from './adam_project.jpg';
import blackLight from './blackLight.jpg';
import black_light from './black_light.jpg';
import noExit from './noExit.jpg';
import no_exit from './no_exit.jpg';
import kingsMan from './kingsMan.jpg';
import kings_man from './kings_man.jpg';
import sing2 from './sing2.jpg';
import sing_2 from './sing_2.jpg';
import offlineImg from "./offlineImg.png";
import loginImage from "./loginImage.jpg";
import registerImage from "./registerImage.png";
import success from "./success.png";

export {
    BookApp,
    Encanto_,
    encanto,
    turningRed,
    turning_red,
    spiderMan,
    spider_man,
    theBatman,
    the_batman,
    adamProject,
    adam_project,
    blackLight,
    black_light,
    noExit,
    no_exit,
    kingsMan,
    kings_man,
    sing2,
    sing_2,
    offlineImg,
    loginImage,
    registerImage,
    success,
};